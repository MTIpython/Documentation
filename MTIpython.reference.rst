MTIpython\.reference package
============================

MTIpython\.reference\.bibliography module
-----------------------------------------

.. automodule:: MTIpython.reference.bibliography
    :members:
    :undoc-members:
    :show-inheritance:
