MTIpython\.mech package
=======================

MTIpython.mech.principal.core module
------------------------------------

.. automodule:: MTIpython.mech.principal.core
    :members:
    :undoc-members:
    :show-inheritance:

    .. autodata:: MTIpython.mech.principal.core.power
    .. autodata:: MTIpython.mech.principal.core.torque
    .. autodata:: MTIpython.mech.principal.core.moment
    .. autodata:: MTIpython.mech.principal.core.stress
    .. autodata:: MTIpython.mech.principal.core.allowable_stress
    .. autodata:: MTIpython.mech.principal.core.yield_safety_factor


MTIpython.mech.principal.axle module
------------------------------------

.. automodule:: MTIpython.mech.principal.axle
    :members:
    :undoc-members:
    :show-inheritance:

    .. autodata:: MTIpython.mech.principal.axle.diameter
    .. autodata:: MTIpython.mech.principal.axle.diameter_prime


MTIpython.mech.gear module
-------------------------------

.. automodule:: MTIpython.mech.gear
    :members:
    :undoc-members:
    :show-inheritance:


MTIpython\.mech\.axle module
----------------------------

.. automodule:: MTIpython.mech.axle
    :members:
    :undoc-members:
    :show-inheritance:


MTIpython\.mech\.auger module
-----------------------------

.. automodule:: MTIpython.mech.auger
    :members:
    :undoc-members:
    :show-inheritance:
