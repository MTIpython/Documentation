MTIpython\.material package
===========================

MTIpython\.material\.materialfactory module
-------------------------------------------

.. automodule:: MTIpython.material.materialfactory
    :members:
    :undoc-members:
    :show-inheritance:

MTIpython\.material\.base module
--------------------------------

.. inheritance-diagram:: MTIpython.material.base.Matter
    :parts: 2
.. autoclass:: MTIpython.material.base.Matter
    :members: __init__, load, save
    :show-inheritance:

.. inheritance-diagram:: MTIpython.material.base.Category
    :parts: 2
.. autoclass:: MTIpython.material.base.Category
    :members:
    :show-inheritance:
    :undoc-members:

MTIpython\.material\.liquid module
----------------------------------

.. inheritance-diagram:: MTIpython.material.liquid.Liquid
    :parts: 2
.. autoclass:: MTIpython.material.liquid.Liquid
    :members: __init__, load, save
    :show-inheritance:

.. inheritance-diagram:: MTIpython.material.liquid.Bingham
    :parts: 2
.. autoclass:: MTIpython.material.liquid.Bingham
    :members: __init__, load, save
    :show-inheritance:

MTIpython\.material\.gas module
-------------------------------

.. inheritance-diagram:: MTIpython.material.gas.Gas
    :parts: 2
.. autoclass:: MTIpython.material.gas.Gas
    :members: __init__, load, save
    :show-inheritance:

MTIpython\.material\.solid module
---------------------------------

.. inheritance-diagram:: MTIpython.material.solid.Solid
    :parts: 2
.. autoclass:: MTIpython.material.solid.Solid
    :members: __init__, load, save
    :show-inheritance:

MTIpython\.material\.metal module
---------------------------------

.. inheritance-diagram:: MTIpython.material.metal.Metal
    :parts: 2
.. autoclass:: MTIpython.material.metal.Metal
    :members: __init__, load, save
    :show-inheritance:

MTIpython\.material\.plastic module
-----------------------------------

.. inheritance-diagram:: MTIpython.material.plastic.Plastic
    :parts: 2
.. autoclass:: MTIpython.material.plastic.Plastic
    :members: __init__, load, save
    :show-inheritance:

MTIpython\.material\.granular module
------------------------------------

.. inheritance-diagram:: MTIpython.material.granular.Granular
    :parts: 2
.. autoclass:: MTIpython.material.granular.Granular
    :members: __init__, load, save
        :show-inheritance:

.. inheritance-diagram:: MTIpython.material.granular.Powder
    :parts: 2
.. autoclass:: MTIpython.material.granular.Powder
    :members: __init__, load, save
    :show-inheritance:

.. autoclass:: MTIpython.material.granular.ParticleSizeDistribution
    :members: __init__
    :show-inheritance:

.. autoclass:: MTIpython.material.granular.GeldartGrouping
    :members: A, B, C, D
    :show-inheritance:

.. autoclass:: MTIpython.material.granular.Configuration
    :members: RLP, JAMMING, RCP
    :show-inheritance:

MTIpython\.material\.core module
--------------------------------

.. automodule:: MTIpython.material.core
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:

.. currentmodule:: MTIpython.material.core
.. automethod:: MTIpython.material.core.volume_from_specific_weight_and_mass(gamma, m)
.. automethod:: MTIpython.material.core.volume_from_specific_volume_and_mass(v, m)
.. automethod:: MTIpython.material.core.volume_from_density_and_mass(rho, m)
.. automethod:: MTIpython.material.core.mass_from_specific_volume_and_volume(v, V)
.. automethod:: MTIpython.material.core.mass_from_specific_weight_and_volume(gamma, V)
.. automethod:: MTIpython.material.core.mass_from_density_and_volume(rho, V)
.. automethod:: MTIpython.material.core.density_from_mass_and_volume(m, V)
.. automethod:: MTIpython.material.core.density_from_specific_weight(gamma)
.. automethod:: MTIpython.material.core.density_from_specific_volume(v)
.. automethod:: MTIpython.material.core.specific_weight_from_mass_and_volume(m, V)
.. automethod:: MTIpython.material.core.specific_weight_from_specific_volume(v)
.. automethod:: MTIpython.material.core.specific_weight_from_density(rho)
.. automethod:: MTIpython.material.core.specific_volume_from_mass_and_volume(m, V)
.. automethod:: MTIpython.material.core.specific_volume_from_density(rho)

