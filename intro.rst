Introduction
============

The MTIpython module is based upon `pyMech <https://github.com/peer23peer/pymech>`_ which was a personal project of
Jelle Spijker released under the MIT license. Both MTIpython and pymech share the same philosophy: **"One should not have
to do things twice"** Why should you have to look up the material specification when you want to do a quick calculation
in Python? Maarten in 't Veld, saw merit in the original pymech project and the project was renamed MTIpython.

.. image:: https://gitlab.com/MTIpython/Documentation/blob/develop/resources/mtipint_example.gif

Current status
--------------
.. image:: https://mti-gitlab.ihc.eu/MTIpython/MTIpython/badges/develop/build.svg
.. image:: https://mti-gitlab.ihc.eu/MTIpython/MTIpython/badges/develop/coverage.svg
.. image:: https://mtipython.pages.mti-gitlab.ihc.eu/MTIpython/pylint/_images/pylint_badge.svg
.. image:: https://img.shields.io/badge/License-MIT-green.svg

The API documentation can be found at
`MTIpython.pages.mti-gitlab.ihc.eu/MTIpython <http://MTIpython.pages.mti-gitlab.ihc.eu/MTIpython>`_

The latest **code coverage report** can be found at:
`MTIpython.pages.mti-gitlab.ihc.eu/MTIpython/coverage <http://MTIpython.pages.mti-gitlab.ihc.eu/MTIpython/coverage>`_

The latest **pylint report** can be found at:
`MTIpython.pages.mti-gitlab.ihc.eu/MTIpython/pylint/pylint.html <http://MTIpython.pages.mti-gitlab.ihc.eu/MTIpython/pylint/pylint.html>`_

Installation
------------

There are two types of installation procedures: User and developer.
The following prerequisites are needed:

* `Python <https://www.python.org/downloads/>`_ **only tested against Python 3.6**

  .. Note::

     for employee's with an **IHC** issued computer it's recommended to use the
     `IHC Python installer <http://mti-gitlab.ihc.eu/generic-software/IHCpython_installer>`_. This installer will also
     install IHC issued certificates, pip and Numpy. Which can be a pain to install on a **IHC** issued windows computer.

* `Git <https://git-scm.com/downloads>`_ **when compiling from source**

Normal user setup
^^^^^^^^^^^^^^^^^

A normal **Non**-development user has two ways of installing MTIpython. He can either choose to download the latest
`wheel <https://mti-gitlab.ihc.eu/MTIpython/MTIpython/-/jobs/artifacts/develop/download?job=release>`_ file.
extract the file ``*``.whl file to a known location and install MTIpython using the command prompt

.. code-block:: bash

    pip install MTIpython-<version nr>-py3-none-any.whl


An other way is to compile MTIpython from source, pulling the code from the repository using git and issuing the
following commands on the command prompt:

.. code-block:: bash

    git clone https://mti-gitlab.ihc.eu/MTIpython/MTIpython.git
    cd MTIpython
    git checkout master
    pip install --trusted-host www.coolprop.dreamhosters.com --process-dependency-links --pre .

Development setup
^^^^^^^^^^^^^^^^^
It's good practice to setup a `Virtual environment <https://virtualenv.pypa.io/en/stable/userguide/>`_ for development
usage.

.. warning::
    Don't use **pip-sync** systemwide, make sure the virtual environment is activated!

Clone the project
"""""""""""""""""

.. code-block:: bash

    git clone https://mti-gitlab.ihc.eu/MTIpython/MTIpython.git
    cd MTIpython
    git checkout develop

Work with virtual environments
""""""""""""""""""""""""""""""

.. code-block:: bash

    pip install virtualenv
    mkdir venv
    virtualenv venv

Activate the virtual environment
""""""""""""""""""""""""""""""""

.. code-block:: bash

    venv\Scripts\activate

Install development packages
""""""""""""""""""""""""""""

.. code-block:: bash

    pip install --trusted-host www.coolprop.dreamhosters.com --process-dependency-links --pre -e .
    pip install -r dev-requirements.txt
