MTIpython's Jupyter examples
============================

The following pages illustrate the use of **MTIpython** using `Jupyter <http://jupyter.org/>`_. All Jupyter examples can
  be found in *MTIpython\Documentation\Examples*

.. toctree::
   :maxdepth: 4

   Examples/axle_calculations.ipynb
   Examples/bibliography.ipynb
   Examples/fluid.ipynb
