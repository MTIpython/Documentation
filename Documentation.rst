Example MTI Style Python Docstrings
===================================

All code is to be documented using [Sphynx](http://www.sphinx-doc.org/en/stable/).
Documentation style is based on the google sphinx example.

.. literalinclude:: example_mti.py
   :language: python


