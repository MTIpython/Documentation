.. only:: html

   .. rubric:: Bibliography

.. bibliography:: refs.bib
    :all:
