Developer documentation
=======================

The following chapter is intended for developers of **MTIpython** it shows the workflow and basic design philosophy.

* Developer should follow the :ref:`style-guide`
* A commit needs to WORK!
* Use the issue system! Feature request and bug fixes are to be reported using the templates
* All public code needs to be documented with Sphinx
* The working of MTIpython is to be guaranteed  using unit tests located in the testsuite
* All functions should work with Pint :class:`.Quantity`, in such a case that the dimensionality of the units are checked
* Source citation, all functions and properties need to have a reference using :func:`.citation`
* Latex output for each function should be present in the comment
* When fixing a bug or adding a feature a merge request needs to be performed
* Merge request are only allowed when all tests are PASSED and all discussions are resolved
* A merge request is always reviewed by somebody else

