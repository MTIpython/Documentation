MTIpython\.mtimath package
==========================

MTIpython\.mtimath\.types module
--------------------------------

.. automodule:: MTIpython.mtimath.types
    :members:
    :undoc-members:
    :show-inheritance:

MTIpython\.mtimath\.geometry module
-----------------------------------

.. automodule:: MTIpython.mtimath.geometry
    :members:
    :undoc-members:
    :show-inheritance:
