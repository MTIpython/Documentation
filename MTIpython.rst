MTIpython package
=================
.. toctree::

    MTIpython.core
    MTIpython.units
    MTIpython.material
    MTIpython.fluid
    MTIpython.mech
    MTIpython.mtimath
    MTIpython.testsuite
    MTIpython.mtiprint
    MTIpython.reference

